import appConfig from '../configs/app.config';

// TODO: Refactor this in the future so only needed language is loaded into bundle
const translations = {
  en: require('../assets/translations/en'),
  pt: require('../assets/translations/pt')
};

function getDictionary () {
  return translations[appConfig.APP_LANG];
}

function translate (key) {
  return getDictionary()[key] || key;
}

export default {
  translate
};
