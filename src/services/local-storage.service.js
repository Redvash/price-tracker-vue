// Local Storage Keys
const localStorageKeys = {
  AUTH_TOKEN_KEY: 'authToken',
  USER_KEY: 'authenticatedUser',
  USER_LISTS_KEY: 'authenticatedUserLists'
};

function getKey (key) {
  let value = localStorage.getItem(key);

  // Attempt to parse value as JSON in case it's an object
  try {
    return JSON.parse(value);
  } catch {
    return value;
  }
}

function setKey (key, value) {
  // If value is object, stringify it
  if (typeof value === 'object') {
    value = JSON.stringify(value);
  }

  return localStorage.setItem(key, value);
}

export default {
  ...localStorageKeys,

  getKey,
  setKey
};
