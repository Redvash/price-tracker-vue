import Vue from 'vue';
import VueRouter from 'vue-router';

import store from '../store';

// Views
import SearchView from '../views/search.view.vue';
import ProductDetailsView from '../views/product-details.view';
import ListsOverviewView from '../views/lists-overview.view';

Vue.use(VueRouter);

const routes = [
  {
    path: '/products',
    name: 'search',
    component: SearchView
  },
  {
    path: '/products/:productId',
    name: 'productDetails',
    component: ProductDetailsView
  },
  {
    path: '/lists',
    name: 'listsOverview',
    component: ListsOverviewView,
    beforeEnter: (to, from, next) => {
      // User can only enter lists page if he is authenticated
      store.getters['auth/isAuthenticated'] ? next() : next(false);
    }
  },
  {
    path: '/',
    redirect: '/products'
  }
];

const router = new VueRouter({
  routes
});

export default router;
