import dictionary from '../services/dictionary.service';

const Retailers = {
  Auchan: 0,
  Continente: 1,
  PingoDoce: 2,

  properties: [
    {
      id: 0,
      name: 'Auchan',
      code: 'Jumbo'
    },
    {
      id: 1,
      name: 'Continente',
      code: 'Continente'
    },
    {
      id: 2,
      name: 'Pingo Doce',
      code: 'MercadaoPingoDoce'
    }
  ]
};
const ProductsSortOptions = {
  Name: 0,
  Price: 1,
  Relevance: 2,
  LastPriceChange: 3,
  LastPriceChangeDate: 4,

  properties: [
    {
      id: 0,
      name: dictionary.translate('sortOptions.Name'),
      code: 'name'
    },
    {
      id: 1,
      name: dictionary.translate('sortOptions.Price'),
      code: 'price'
    },
    {
      id: 2,
      name: dictionary.translate('sortOptions.Relevance'),
      code: ''
    },
    {
      id: 3,
      name: dictionary.translate('sortOptions.LastPriceChangeDate'),
      code: 'last_price_change_date'
    },
    {
      id: 4,
      name: dictionary.translate('sortOptions.LastPriceChange'),
      code: 'last_price_change'
    }
  ]
};
const ValidationRules = {
  Required: 0,
  Email: 1,
  Number: 2,

  properties: [
    {
      id: 0,
      name: 'Required',
      message: dictionary.translate('validationRules.RequiredMessage')
    },
    {
      id: 1,
      name: 'Email',
      message: dictionary.translate('validationRules.EmailMessage')
    },
    {
      id: 2,
      name: 'Number',
      message: dictionary.translate('validationRules.NumberMessage')
    }
  ]
};
const BackendErrors = {
  InvalidCredentials: 0,
  EmailAlreadyTaken: 1,

  properties: [
    {
      id: 0,
      message: dictionary.translate('backendErrors.InvalidCredentials')
    },
    {
      id: 1,
      message: dictionary.translate('backendErrors.EmailAlreadyTaken')
    }
  ]
};
const MeasurementUnits = {
  KG: 0,
  G: 1,
  L: 2,
  UN: 3,
  DOSE: 4,

  properties: [
    {
      id: 0,
      name: 'KG',
      code: 'kg',
      downgradeUnit: 'G',
      downgradeRatio: 1000
    },
    {
      id: 1,
      name: 'G',
      code: 'g',
      downgradeUnit: 'MG',
      downgradeRatio: 1000
    },
    {
      id: 2,
      name: 'L',
      code: 'l',
      downgradeUnit: 'ML',
      downgradeRatio: 1000
    },
    {
      id: 3,
      name: 'UN',
      code: 'un',
      downgradeUnit: null,
      downgradeRatio: null
    },
    {
      id: 4,
      name: 'DOSE',
      code: 'dose',
      downgradeUnit: null,
      downgradeRatio: null
    }
  ]
};

const getEnumPropertiesByCode = (selectedEnum, code) => {
  return selectedEnum.properties.find(enumEntry => enumEntry.code === code);
};

export {
  Retailers,
  ProductsSortOptions,
  ValidationRules,
  BackendErrors,
  MeasurementUnits,

  getEnumPropertiesByCode
};
