export default {
  API_URL: 'http://localhost:8000', // Development
  // API_URL: 'https://price-tracker-dijojo.herokuapp.com', // Production

  APP_LANG: 'pt',
  USE_FIXTURES: false // Used to mock api calls
};
