import Vue from 'vue';
import App from './App.vue';
import router from './configs/router.config';
import store from './store';

// All application styles
import './assets/sass/index.scss';

// External Global Javascript libraries
import 'jquery/dist/jquery.min';
import 'popper.js/dist/popper.min';
import 'bootstrap/dist/js/bootstrap.min';

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
