import moment from 'moment';

import { Retailers, MeasurementUnits, getEnumPropertiesByCode } from '../configs/enums.config';

import RootModel from './_root.model';

export default class Product extends RootModel {
  constructor (data = {}) {
    super(data);

    // Only process data if it's not a model
    if (!data._isModel) {
      this.id = data.id || null;
      this.name = data.name || '';
      this.price = parseFloat(data.price || 0).toFixed(2);
      this.imageUrl = data.image_url || '';
      this.retailer = getEnumPropertiesByCode(Retailers, data.retailer).name || null;

      this.generateMeasurementFields(data.measurement, data.measurement_units);
      this.generatePriceHistory(data.price_change_event_log);
    }
  }

  generateMeasurementFields (measurement = 1, measurementUnit = 'un') {
    const unitProperties = getEnumPropertiesByCode(MeasurementUnits, measurementUnit);

    this.measurement = parseFloat(measurement);
    this.measurementUnit = unitProperties.name;
    this.displayMeasurement = this.measurement;
    this.displayMeasurementUnit = this.measurementUnit;
    this.pricePerMeasurement = (this.price / this.measurement).toFixed(2);

    if (this.measurement < 1 && unitProperties.downgradeUnit) {
      this.displayMeasurement *= unitProperties.downgradeRatio;
      this.displayMeasurementUnit = unitProperties.downgradeUnit;
    }
  }

  generatePriceHistory (history = []) {
    this.priceHistory = history.map(entry => ({
      id: entry.id,
      price: entry.price,
      priceDifference: entry.price_change,
      priceDate: moment(entry.price_change_date, 'X')
    }));
  }
}
