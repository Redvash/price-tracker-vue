import RootModel from './_root.model';

export default class User extends RootModel {
  constructor (data) {
    super(data);

    // Only process data if it's not a model
    if (!data._isModel) {
      this.id = data.id || null;
      this.name = data.name || 'Jonh Doe';
      this.email = data.email || null;
    }
  }
}
