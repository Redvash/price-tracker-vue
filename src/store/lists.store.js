// Services
import dictionary from '../services/dictionary.service';
import logger from '../services/logger.service';
import localStorageService from '../services/local-storage.service';

// Resources
import productsResource from '../resources/products.resource';
import listsResource from '../resources/lists.resource';

export default {
  name: 'lists',
  namespaced: true,
  state: {
    loading: false,

    lists: localStorageService.getKey(localStorageService.USER_LISTS_KEY) || []
  },
  actions: {
    initialize () {
      return Promise.resolve();
    },

    loadLists ({ state, commit }) {
      commit('setLoading', {
        loading: true
      });

      const promises = [];
      const lists = state.lists.slice();

      lists.forEach(list => {
        list.tracked_products.forEach((product, productIndex) => {
          const promise = productsResource.getProductDetails(product.id)
            .then(product => {
              list.tracked_products[productIndex] = product;
            });

          promises.push(promise);
        });
      });

      return Promise.all(promises)
        .then(() => {
          commit('setLists', { lists });
          commit('setLoading', {
            loading: false
          });
        });
    },

    addItemToList ({ state, commit }, { listId, productId }) {
      // Check if given list exists
      const listIndex = state.lists.findIndex(list => list.id === listId);
      if (listIndex === -1) {
        logger.logError('listsStore : addItemToList() => given list id does not exist', listId);
        return;
      }

      commit('setLoading', {
        loading: true
      });

      return listsResource.addProductsToList(listId, [productId])
        .then(result => {
          logger.logSuccess(dictionary.translate('listsStore.ItemAddedSuccessfully'), result, true);
        })
        .catch(error => {
          logger.logError(dictionary.translate('listsStore.ErrorAddingItem') + error.message, error, true);
        })
        .finally(() => {
          commit('setLoading', {
            loading: false
          });
        });
    },
    addItemToNewList ({ state, commit, dispatch }, { listName, productId }) {
      commit('setLoading', {
        loading: true
      });

      return listsResource.createList(listName)
        .then(result => {
          commit('addList', result);
          dispatch('addItemToList', {
            listId: result.id,
            productId
          });
        })
        .catch(error => {
          logger.logError(dictionary.translate('listsStore.ErrorCreatingList') + error.message, error, true);
        })
        .finally(() => {
          commit('setLoading', {
            loading: false
          });
        });
    },

    deleteList ({ state, commit }, { listId }) {
      commit('setLoading', {
        loading: true
      });

      return listsResource.deleteList(listId)
        .then(() => {
          commit('removeList', { listId });
        })
        .catch(error => {
          logger.logError(dictionary.translate('listsStore.ErrorDeletingList') + error.message, error, true);
        })
        .finally(() => {
          commit('setLoading', {
            loading: false
          });
        });
    }
  },
  mutations: {
    setLoading (state, { loading }) {
      state.loading = loading;
    },

    setLists (state, { lists }) {
      state.lists = lists;
      localStorageService.setKey(localStorageService.USER_LISTS_KEY, state.lists);
    },
    addList (state, { list }) {
      state.lists.push(list);
      localStorageService.setKey(localStorageService.USER_LISTS_KEY, state.lists);
    },
    removeList (state, { listId }) {
      const selectedListIndex = state.lists.findIndex(list => list.id === listId);
      const listsClone = state.lists.slice();

      if (selectedListIndex !== -1) {
        listsClone.splice(selectedListIndex, 1);
        state.lists = listsClone;
      }
    },

    addProductToList (state, { listId, product }) {
      const selectedList = state.lists.find(list => list.id === listId);

      if (selectedList) {
        selectedList.tracked_products.push(product);
        localStorageService.setKey(localStorageService.USER_LISTS_KEY, state.lists);
      }
    }
  }
};
