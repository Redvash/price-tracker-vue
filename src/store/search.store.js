import productsResource from '../resources/products.resource';

export default {
  name: 'search',
  namespaced: true,
  state: {
    filterName: '',
    filterVendor: '',
    filterSort: '',
    filterSortOrder: 'desc',

    loadingProducts: false,

    products: [],
    totalProducts: 100,
    pageSize: 30,
    currentPage: 1,
    totalPages: 10
  },
  actions: {
    initialize (context) {
      context.dispatch('loadProducts');
    },

    loadProducts (context) {
      context.commit('setLoading', {
        loading: true
      });

      let from = (context.state.currentPage - 1) * context.state.pageSize;

      productsResource.getProducts(
        from,
        context.state.pageSize,
        context.state.filterName,
        context.state.filterVendor,
        context.state.filterSort,
        context.state.filterSortOrder
      )
        .then(result => {
          context.commit('productsLoaded', result);
          context.commit('setLoading', {
            loading: false
          });
        });
    },

    changeNameFilter (context, { name }) {
      context.commit('setNameFilter', {
        name
      });
      context.commit('setCurrentPage', {
        currentPage: 1
      });

      setTimeout(() => {
        if (context.state.filterName === name) {
          context.dispatch('loadProducts');
        }
      }, 500);
    },
    changeRetailerFilter (context, { retailer }) {
      context.commit('setRetailerFilter', {
        retailer
      });
      context.commit('setCurrentPage', {
        currentPage: 1
      });

      context.dispatch('loadProducts');
    },
    changeSort (context, { sort }) {
      context.commit('setSort', {
        sort
      });
      context.commit('setCurrentPage', {
        currentPage: 1
      });

      context.dispatch('loadProducts');
    },
    changeSortOrder (context, { sortOrder }) {
      context.commit('setSortOrder', {
        sortOrder
      });
      context.commit('setCurrentPage', {
        currentPage: 1
      });

      context.dispatch('loadProducts');
    },

    changeCurrentPage (context, { currentPage }) {
      context.commit('setCurrentPage', {
        currentPage
      });

      context.dispatch('loadProducts');
    }
  },
  mutations: {
    setLoading (state, { loading }) {
      state.loadingProducts = loading;
    },

    productsLoaded (state, { products, total }) {
      state.products = products;
      state.totalProducts = total;
      state.totalPages = Math.ceil(total / state.pageSize);
    },

    setNameFilter (state, { name }) {
      state.filterName = name;
    },
    setRetailerFilter (state, { retailer }) {
      state.filterVendor = retailer;
    },
    setSort (state, { sort }) {
      state.filterSort = sort;
    },
    setSortOrder (state, { sortOrder }) {
      state.filterSortOrder = sortOrder;
    },

    setCurrentPage (state, { currentPage }) {
      state.currentPage = currentPage;
    }
  }
};
