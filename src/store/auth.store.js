// Services
import dictionary from '../services/dictionary.service';
import logger from '../services/logger.service';
import localStorageService from '../services/local-storage.service';

// Resources
import authResource from '../resources/auth.resource';

export default {
  name: 'auth',
  namespaced: true,
  state: {
    loading: false,

    currentUser: localStorageService.getKey(localStorageService.USER_KEY) || null
  },
  getters: {
    isAuthenticated (state) {
      return state.currentUser !== null;
    }
  },
  actions: {
    initialize () {
      return Promise.resolve();
    },

    authenticateUser ({ commit }, { email, password }) {
      commit('setLoading', {
        loading: true
      });

      return authResource.authenticateUser(email, password)
        .then(({ name, email, lists }) => {
          commit('setCurrentUser', {
            user: {
              name,
              email
            }
          });
          commit('lists/setLists', { lists }, { root: true });

          logger.logSuccess(dictionary.translate('authStore.UserAuthenticatedSuccessfully'), {
            name,
            email,
            lists
          }, true);
        })
        .catch(error => {
          logger.logError(dictionary.translate('authStore.ErrorAuthenticatingUser') + error.message, error, true);
        })
        .finally(() => {
          commit('setLoading', {
            loading: false
          });
        });
    },
    logoutUser ({ commit }) {
      commit('setCurrentUser', {
        user: null
      });
      commit('lists/setLists', { lists: [] }, { root: true });

      logger.logSuccess(dictionary.translate('authStore.LogoutSuccessful'), null, true);
    },
    registerUser ({ commit }, { name, email, password }) {
      commit('setLoading', {
        loading: true
      });

      return authResource.registerUser(name, email, password)
        .then(result => {
          commit('setCurrentUser', {
            user: result
          });

          logger.logSuccess(dictionary.translate('authStore.UserRegisteredSuccessfully'), result, true);
        })
        .catch(error => {
          logger.logError(dictionary.translate('authStore.ErrorRegisteringUser') + error.message, error, true);
        })
        .finally(() => {
          commit('setLoading', {
            loading: false
          });
        });
    }
  },
  mutations: {
    setLoading (state, { loading }) {
      state.loading = loading;
    },

    setCurrentUser (state, { user }) {
      state.currentUser = user;
      localStorageService.setKey(localStorageService.USER_KEY, user);
    }
  }
};
