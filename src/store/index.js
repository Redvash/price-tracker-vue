import Vue from 'vue';
import Vuex from 'vuex';
import $ from 'jquery';

Vue.use(Vuex);

const modules = [
  require('./search.store').default,
  require('./auth.store').default,
  require('./lists.store').default
];
const store = new Vuex.Store({});

// Register modules and wait for initialization
const initializationPromises = modules.map(module => {
  store.registerModule(module.name, module);
  return store.dispatch(module.name + '/initialize');
});

// When all modules have finished initializing, remove global app loading overlay
Promise.all(initializationPromises)
  .then(() => {
    $('#app-loading-overlay').addClass('ready');
  });

export default store;
