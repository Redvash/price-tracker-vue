// Services
import localStorageService from '../services/local-storage.service';
import httpService from '../services/http.service';

// TODO: Implement proper logic when backend is ready
function getCurrentUser () {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(localStorageService.getKey(localStorageService.USER_KEY));
    }, 1000);
  });
}

function authenticateUser (userEmail, userPassword) {
  const data = {
    email: userEmail,
    password: userPassword
  };

  return httpService.post('users/login', undefined, data)
    .then(response => {
      if (response.auth) {
        const authenticatedUser = {
          name: 'Jonh Doe', // TODO: Add new user name field on backend
          email: response.user.email,
          lists: response.user.tracked_lists
        };

        localStorageService.setKey(localStorageService.AUTH_TOKEN_KEY, response.token);

        return authenticatedUser;
      }
    });
}

function registerUser (userName, userEmail, userPassword) {
  const data = {
    email: userEmail,
    password: userPassword
  };

  return httpService.post('users/register', undefined, data)
    .then(response => {
      return response.email;
    });
}

export default {
  getCurrentUser,
  authenticateUser,
  registerUser
};
