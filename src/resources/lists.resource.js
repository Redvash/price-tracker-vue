// Services
import httpService from '../services/http.service';

function createList (listName) {
  const data = {
    list_name: listName
  };

  return httpService.post('users/lists', undefined, data);
}

function deleteList (listId) {
  const data = {
    list_id: listId
  };

  return httpService.delete('users/lists', undefined, data);
}

function addProductsToList (listId, productIds) {
  const url = `users/lists/${listId}/products`;
  const data = {
    product_ids: productIds
  };

  return httpService.post(url, undefined, data);
}

function deleteProductsToList (listId, productIds) {
  const url = `users/lists/${listId}/products`;
  const data = {
    product_ids: productIds
  };

  return httpService.delete(url, undefined, data);
}

export default {
  createList,
  deleteList,
  addProductsToList,
  deleteProductsToList
};
