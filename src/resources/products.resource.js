// Fixtures
import productsData from '../fixtures/products';
import productDetailsData from '../fixtures/productDetails';

// Config
import appConfig from '../configs/app.config';

// Services
import httpService from '../services/http.service';

// Models
import ProductModel from '../models/product.model';

function getProducts (from, pageSize, nameFilter, retailerFilter, sortBy, sortOrder) {
  let params = {
    from: from,
    pageSize: pageSize,
    name: nameFilter,
    retailer: retailerFilter,
    sortBy: sortBy,
    sortOrder: sortOrder
  };

  return (
    appConfig.USE_FIXTURES
      ? Promise.resolve(productsData)
      : httpService.get('products', params)
  )
    .then(data => {
      return {
        products: data.hits.map(entry => new ProductModel(entry.product)),
        total: data.total
      };
    });
}

function getProductDetails (productId) {
  return (
    appConfig.USE_FIXTURES
      ? Promise.resolve(productDetailsData)
      : httpService.get(`products/${productId}`)
  )
    .then(data => new ProductModel(data[0]));
}

export default {
  getProducts,
  getProductDetails
};
