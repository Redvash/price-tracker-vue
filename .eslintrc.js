module.exports = {
  root: true,
  env: {
    node: true
  },
  'extends': [
    'plugin:vue/essential',
    '@vue/standard'
  ],
  rules: {
    "no-console": "off",
    "no-var": "error",
    "key-spacing": [
      "warn",
      {
        "beforeColon": false,
        "afterColon": true
      }
    ],
    "keyword-spacing": "warn",
    "comma-spacing": "warn",
    "object-curly-spacing": "warn",
    "eqeqeq": "error",
    "no-multi-spaces": "warn",
    "space-infix-ops": "warn",
    "no-unused-vars": "warn",
    "no-process-exit": "off",
    "no-multiple-spaces": "off",
    'indent': 'off',
    'vue/script-indent': ['warn', 2, {
      'baseIndent': 1
    }],
    "semi": ["error", "always"],
    "brace-style": "off",
    "vue/order-in-components": "warn"
  },
  parserOptions: {
    parser: 'babel-eslint'
  }
}
