module.exports = {
  devServer: {
    port: 3000
  },
  css: {
    loaderOptions: {
      sass: {
        prependData: `
          @import '~bootstrap/scss/functions';
          @import '~bootstrap/scss/mixins';
          @import '~bootstrap/scss/variables';
          @import '~bootstrap/scss/utilities/spacing';
        `
      }
    }
  }
};
